package edu.uprm.cse.datastructures.cardealer;


import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;                                                   
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

/*
 * In this class you can see how CarManager is implemented.
 * This class uses different elements to managed the cars in a list of cars.
 * Created by: Yaritza M. Garcia Chaparro
*/

@Path("/cars")
public class CarManager {
	
	private final CircularSortedDoublyLinkedList<Car> carList = CarList.getInstance();
	
	//Return all the elements in the carList.
	@GET
	//@Path("/cars")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getCarList() {
		Car[] carArr = new Car[carList.size()];
		
		for (int i = 0; i < carList.size(); i++) {
			carArr[i] = carList.get(i);
		}
		return carArr;
	}
	
	//Returns the car that has the given id.
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){
		
		for (int i = 0; i < carList.size(); i++) {
			if(carList.get(i).getCarId() == id) {
				return carList.get(i);
			}
		}
		throw new NotFoundException();
	}  
	
	//Add a new car to the CarList
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
    public Response createCar(Car car){

      carList.add(car);
      return Response.status(201).build();
    }     
	
	//Delete the car that has the given id.
	@DELETE
    @Path("/{id}/delete")
    public Response deleteCar(@PathParam("id") long id){
    	for (Car car : carList) {
			if(car.getCarId() == id) {
				carList.remove(car);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(404).build();
  
    }  
	
	//Update the given car in the list. In other words, delete a car with the same id as the given one in the list and add the given car.
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car){
		for(int i=0; i < carList.size(); i++){
			if(carList.get(i).getCarId() == car.getCarId()){
				carList.remove(i);
				carList.add(car);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(404).build();
	}
	
}
