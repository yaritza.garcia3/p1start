package edu.uprm.cse.datastructures.cardealer;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

/*
 * In this class you can see how CarList is implemented.
 * Created by: Yaritza M. Garcia Chaparro
*/
public class CarList {

	private static CircularSortedDoublyLinkedList<Car> list = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	
	//Remove all the cars in the list.
	public static void resetCars() {
		list.clear();	
	}
	
	//Return a CircularSortedDoublyLinkedList of cars;
	public static CircularSortedDoublyLinkedList<Car> getInstance() {
		return list;
	}

}

