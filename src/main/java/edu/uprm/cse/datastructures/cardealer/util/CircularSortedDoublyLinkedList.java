package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*In this class you can see how CircularSortedDoublyLinkedList is implemented.
 Created by: Yaritza M. Garcia Chaparro*/
public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

	/*
	 * This class is an inner class of CircularSortedDoublyLinkedList.
	 * In this class is the Iterator of the CircularSortedDoublyLinkedList.
	 */
	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E> {
		private DoublyNode<E> nextNode;
		
		//Constructor
		public CircularSortedDoublyLinkedListIterator() {
			this.nextNode = (DoublyNode<E>) header.getNext();
		}

		//This method verify if the node where it is has next node. 
		@Override
		public boolean hasNext() {
			return nextNode != null;
		}
		//Returns the next element in the iteration.
		@Override
		public E next() {
			if (this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			} else {
				throw new NoSuchElementException();
			}
		}

	}
	/*
	 * This class is an inner class of CircularSortedDoublyLinkedList.
	 * In this class you can see how DoublyNode is implemented.
	 */

	private static class DoublyNode<E> {
		private E element; //The element of the DoublyNode
		private DoublyNode<E> next; //The element that goes after this.
		private DoublyNode<E> prev; //The element that goes before this.

		//Constructors
		public DoublyNode(E element) {
			this.element = element;
		}
		public DoublyNode(E element, DoublyNode<E> next, DoublyNode<E> prev) {
			super();
			this.element = element;
			this.next = next;
			this.prev = prev;
		}

		public DoublyNode() {
			super();
		}

		//Getters
		public E getElement() {
			return element;
		}
		
		public DoublyNode<E> getNext() {
			return next;
		}

		public DoublyNode<E> getPrevious() {
			return prev;
		}
		
		//Setters
		public void setElement(E element) {
					this.element = element;
				}
		public void setNext(DoublyNode<E> next) {
			this.next = next;
		}

		public void setPrevious(DoublyNode<E> prev) {
			this.prev = prev;
		}

	}

	private DoublyNode<E> header; //This node is null and his next node is the first one in the CircularSortedDoublyLinkedList.
	private int currentSize; //The number of elements that the CircularSortedDoublyLinkedList has.
	private Comparator comparator;

	//Constructor
	public CircularSortedDoublyLinkedList(Comparator comp) {
		header = new DoublyNode<>();
		currentSize = 0;
		this.comparator = comp;
	}

	@Override
	public Iterator<E> iterator() {
		return new CircularSortedDoublyLinkedListIterator<E>();
	}

	//This method adds a new node to the CircularSortedDoublyLinkedList.
	@Override
	public boolean add(E obj) {

		DoublyNode<E> newOne = new DoublyNode<E>(obj,this.header, this.header);
		//If the CircularSortedDoublyLinkedList is empty the add the element after the header.
		if (this.isEmpty()) {
			this.header.setNext(newOne);
			this.header.setPrevious(newOne);
			currentSize++;
			return true;
		}

		else {
			DoublyNode<E> temp = this.header.next;
			while(temp != header) {
				//If the element where I am is greater than the given element then add the new node.
				if(this.comparator.compare(newOne.element, temp.getElement()) < 0) {
					newOne.setNext(temp);
					newOne.setPrevious(temp.getPrevious());
					temp.getPrevious().setNext(newOne);
					temp.setPrevious(newOne);
					currentSize++;
					return true;
				}
				temp = temp.getNext();
			}
			//In case that there is no element greater than the given one then add the new node at the end of the CircularSortedDoublyLinkedList.
			newOne.setPrevious(header.getPrevious());
			this.header.getPrevious().setNext(newOne);
			this.header.setPrevious(newOne);
			currentSize++;
			return true;	
			}
			
		}

	//This method returns how many elements the CircularSortedDoublyLinkedList has.
	@Override
	public int size() {
		return this.currentSize;
	}

	//This method removes an element if and only if the element is in the CircularSortedDoublyLinkedList.
	@Override
	public boolean remove(E obj) {
		if (this.currentSize == 0) {
			return false;
		}
		for (DoublyNode<E> temp = this.header.next; temp != header; temp = temp.getNext()) {
			if (temp.element.equals(obj)) {
				temp.prev.setNext(temp.next);
				temp.next.setPrevious(temp.prev);
				this.currentSize--;
				return true;
			}
		}
		return false;
	}

	//This method removes the element in a particular position. 
	@Override
	public boolean remove(int index) {
		//If the CircularSortedDoublyLinkedList is empty or the given position is greater than the number of elements, then throw an IndexOutOfBoundsException.
		if (this.currentSize == 0 || index >= this.currentSize) { 
			throw new IndexOutOfBoundsException();
		}
		DoublyNode<E> temp = this.header.getNext();
		for (int i = 0; i < this.currentSize; i++) {
			if (i == index) {
				temp.prev.setNext(temp.next);
				temp.next.setPrevious(temp.prev);
				this.currentSize--;
				return true;
			}
			temp = temp.getNext();
		}
		return false;
	}

	//This method removes all the elements that are equals to the given element.
	@Override
	public int removeAll(E obj) {
		int count = 0;
		while (this.remove(obj)) {
			count++;
		}
		return count;
	}

	//Returns the first element.
	@Override
	public E first() {
		return this.header.next.getElement();
	}

	//Returns the last element.
	@Override
	public E last() {
		return this.header.prev.getElement();
	}

	//Return the element in the given position
	@Override
	public E get(int index) {
		if (this.currentSize == 0 || index >= this.currentSize) {
			throw new IndexOutOfBoundsException();
		}
		DoublyNode<E> temp = this.header.getNext();
		for (int i = 0; i < this.currentSize; i++) {
			if (i == index) {
				return temp.getElement();
			}
			temp = temp.getNext();
		}
		return null;
	}

	//This method removes all the elements in the CircularSortedDoublyLinkedList.
	@Override
	public void clear() {
		for (int i = 0; i < this.currentSize; i++) {
			this.remove(0);
			i--;
		}
	}

	//Return true if the given element is in the CircularSortedDoublyLinkedList, false otherwise.
	@Override
	public boolean contains(E e) {
		return this.firstIndex(e) >= 0;
	}

	//Return true if the CircularSortedDoublyLinkedList has no elements, false otherwise.
	@Override
	public boolean isEmpty() {
		return this.currentSize == 0;
	}

	//Return the index of the first instance of the given element. If the CircularSortedDoublyLinkedList has no elements returns -1.
	@Override
	public int firstIndex(E e) {
		if (this.currentSize == 0) {
			return -1;
		}
		DoublyNode<E> temp = this.header.getNext();
		for (int i = 0; i < this.currentSize; i++) {
			if (temp.element.equals(e)) {
				return i;
			}
			temp = temp.next;
		}
		return -1;
	}

	//Return the index of the last instance of the given element. If the CircularSortedDoublyLinkedList has no elements returns -1.
	@Override
	public int lastIndex(E e) {
		if (this.currentSize == 0) {
			return -1;
		}
		DoublyNode<E> temp = this.header.getPrevious();
		for (int i = this.currentSize - 1; i >= 0; i--) {
			if (temp.element.equals(e)) {
				return i;
			}
			temp = temp.getPrevious();
		}
		return -1;
	}

}