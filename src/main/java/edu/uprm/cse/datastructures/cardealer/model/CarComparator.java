package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

/*
 * In this class you can see how CarComparator is implemented.
 * This class compares two cars and decided which one is greater based in the Brand, Model and Model Option of the car.
 * Returns 0 if both cars are equals. If the first car is greater than the second car return a number > 0, otherwise return a number < 0.
 * Created by: Yaritza M. Garcia Chaparro
*/
public class CarComparator<E> implements Comparator<E> {
	@Override
	public int compare(E c1, E c2) {
		Car o1 = (Car) c1;
		Car o2 = (Car) c2;
			
		if(o1.getCarBrand().equals(o2.getCarBrand())) {
			if(o1.getCarModel().equals(o2.getCarModel())) {
				return o1.getCarModelOption().compareTo(o2.getCarModelOption());
			}
			return o1.getCarModel().compareTo(o2.getCarModel());
		}
		
		return o1.getCarBrand().compareTo(o2.getCarBrand());
	}

}